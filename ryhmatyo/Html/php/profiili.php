<?php
session_start();
if(isset($_SESSION['id']) && isset($_SESSION['user_name'])) {
    ?>


<!DOCTYPE html>
<html lang="fi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../Css/style.css">
    <title>Profiilisivu</title>
    <script src="../Javascript/script.js"></script>
</head>
<body class="tausta">

    <div class="topnav">
        <div class="profile-link">
            <img id="profile-pic-nav" src="../kuvia/pfp.png" onchange="loadFile(event)" alt="Profile Picture">
        </div>                                  
        <a class="profiili" href="profiili.php"><?php echo $_SESSION['user_name']; ?></a> <!--Laittaa sisäänkirjautuneen nimen näytille-->
        <a href="blogi.php">Blogit</a>
        
    </div>

<div class="rofiili">
    <div class="boksi">
        <h1>Tervetuloa Profiiliisi!</h1>
        

        <img id="profile-pic" src="../kuvia/pfp.png" alt="Profile Picture">
        <div class="profile-information">
            <label for="kayttaja"><h2><?php echo $_SESSION['user_name']; ?></h2><br></label><br> <!--Laittaa sisäänkirjautuneen nimen näytille-->
            <span id="kayttaja"></span><br>
        </div>
        <label for="file" id="change-picture-btn">Vaihda profiilikuva</label>
        <input id="file" type="file" onchange="loadFile(event)" style="display: none;"/><br><br>
        
        <a id="logout" href="logout.php">Kirjaudu ulos</a>
    </div>
    </div>

    <footer>
        <p>Blogisivu</p>
    </footer>
</body>
</html>

<?php
}

else {
    header("Location: kirjautuminen.php");
    exit();
}
?>
