<?php
session_start();
if(isset($_SESSION['id']) && isset($_SESSION['user_name'])) {
    ?>

<!DOCTYPE html>
<html lang="fi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../Css/style.css">
    <title>Blogi</title>
    <script src="../Javascript/script.js"></script>
</head>
<body class="tausta">

<div class="topnav">
    <div class="profile-link">
    <img id="profile-pic-nav" src="../kuvia/pfp.png" onchange="loadFile(event)" alt="Profile Picture">
    </div>
    <a class="profiili" href="profiili.php"><?php echo $_SESSION['user_name']; ?></a>
   
    <a href="blogi.php">Blogit</a>
   
</div>

    <h1>Tervetuloa blogiini</h1>
    <p>Blogeja sivulla: <span id="post-counter">0</span></p>
    <br>

    <div class="blog-container">
        <ul id="blog-posts"></ul>

        <a href="#" id="new-post-link">Luo uusi postaus</a>

        <div id="new-post-form" style="display: none;">
            <h2>Luo uusi postaus</h2>
            <label for="title">Otsikko:</label>
            <input type="text" id="title" class="input-field" required><br>

            <label for="content">Sisältö:</label>
            <textarea id="content" class="comment-textarea" required></textarea>
            <br>
            <button id="publish-button" class="buttoni">Julkaise</button>
        </div>

        <div id="edit-form" style="display: none;">
            <h2>Muokkaa blogia</h2>
            <label for="edit-title">Otsikko:</label>
            <input type="text" id="edit-title" placeholder="Blog Title">
            <br>
            <label for="edit-content">Sisältö:</label>
            <textarea id="edit-content" placeholder="Blog Content"></textarea>
            <button id="update-button">Päivitä blogi</button>
          </div>
    </div>

    <footer>
        <p>Blogisivu</p>
    </footer>

</body>
</html>
<?php
}